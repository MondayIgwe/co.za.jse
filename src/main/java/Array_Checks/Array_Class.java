package Array_Checks;


import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;

public class Array_Class {

    private final String studentInfoData = System.getProperty("user.dir") + File.separator + "/src/main/java/Array_Checks/studentData.xlsx";
    private int[] oneDimensionArray;
    private int[][] twoDimensionArray;
    private File studentsInfo;
    private FileInputStream in;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;


    @Test
    public void arrayDemo() throws IOException {
        int count;

        ////////////One dimension ////////
        oneDimensionArray = new int[5];
        oneDimensionArray[0] = 1;
        oneDimensionArray[1] = 2;
        oneDimensionArray[2] = 3;
        oneDimensionArray[3] = 4;
        oneDimensionArray[4] = 5;

        //Normal For Loop
        for (int i = 0; i < oneDimensionArray.length; i++) {
            count = oneDimensionArray[i];
            System.out.println(count);
        }

        ///two dimension array///////////////
        twoDimensionArray = new int[3][3]; //Size

        //First Row Index
        twoDimensionArray[0][0] = 200;
        twoDimensionArray[0][1] = 300;

        //Second Row Index
        twoDimensionArray[1][0] = 5000;
        twoDimensionArray[1][1] = 3787;

        //Third Row Index
        twoDimensionArray[2][0] = 90899;
        twoDimensionArray[2][1] = 6000;

        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::");
        for (int row = 0; row < twoDimensionArray.length; row++) {
            for (int col = 0; col < twoDimensionArray.length; col++) {
                System.out.println(twoDimensionArray[row] + "\n" + twoDimensionArray[col]);
            }

        }

        //Two dimension Continuation
        int[][] tweD = {
                {2, 3, 4},
                {5, 6, 7},
                {4, 96, 3}
        };

        for (int r = 0; r < tweD.length; r++) {
            for (int c = 0; c < tweD.length; c++) {
                System.out.println("\nTwoD: " + tweD[r][c]);
            }
        }

        //Enhanced forloop
        for (int i : oneDimensionArray) {
            System.out.println("\nEnhanced for loop:  " + i);
        }

    }


    ///////////Read Excel and Pass Data to Two dimension Array Object//////////////////
    public Object[][] getStudentData() throws IOException {
        //Two dimension Continuation
        //Read the student spreadsheet
        studentsInfo = new File(studentInfoData);
        in = new FileInputStream(studentsInfo);
        workbook = new XSSFWorkbook(in);
        sheet = workbook.getSheet("StudentDetails");
        int rowCount = sheet.getPhysicalNumberOfRows();
        int cellCount = sheet.getRow(0).getPhysicalNumberOfCells();

        //Pass the Rows and Columns Two dimension array
        Object[][] studentData = new Object[rowCount][cellCount];

        for (int studentRow = 0; studentRow < rowCount; studentRow++) {
            for (int studentCol = 0; studentCol < cellCount; studentCol++) {

                //Format data when fetched from excel sheet
                DataFormatter formatter = new DataFormatter();
                String format = formatter.formatCellValue(sheet.getRow(studentRow).getCell(studentCol));
                //System.out.println("Student Info: " + format.trim());

                //Pass the rows and columns returned to the Two dimension array
                studentData[studentRow][studentCol] = format;
            }
        }
        in.close();
        workbook.close();
        return studentData;
    }

    @DataProvider
    public Object[][] studentDataCollection() throws IOException {
        Object[][] getStudentDataFomExcel =  getStudentData();
        return getStudentDataFomExcel;
    }

    @Test(dataProvider ="studentDataCollection")
    public void runStudentDataTest(String studentID, String studentName, String studentLastName, String marks){
        System.out.println("id: "+ studentID + ", name: "+ studentName + ", last name: "+ studentLastName + ", marks: "+ marks);
    }


}
