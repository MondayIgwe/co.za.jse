package Assertions;

import org.testng.asserts.SoftAssert;

public class Assertions_Concept {

    public void softAssertTrue(boolean b){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(b);
    }
}
