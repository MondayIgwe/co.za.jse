import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.*;
import static java.lang.Integer.MAX_VALUE;

public class Java_util_Concept {

    Collection c = new ArrayList();
    List list = new LinkedList();

    @Test
    public void test(){
        javaUtils();
    }

    public void javaUtils(){
        c.add("boo");
        c.add(new Integer(98));
        Iterator it = c.iterator();

        while (it.hasNext()){
            System.out.println(it.next().toString());
            System.out.println("The size is: "+c.size() );
        }

        list.addAll(c);
        System.out.println(list);
        System.out.println("\n"+c.spliterator().trySplit());

        if((MAX_VALUE > 1) && (MAX_VALUE != 7)){
            SoftAssert softCheck = new SoftAssert();
            softCheck.assertTrue(true,"Value");
            softCheck.assertEquals("June","June");
            softCheck.assertEquals(2 * 2, 4);
            System.out.println("Max static import: "+MAX_VALUE);
            softCheck.assertAll();
        }

    }
}
