package excelFactory;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class Test_DataProvider {


   @Parameters({"environment"})
    @org.testng.annotations.Test(dataProvider = "getData")
    public void getN(String firstname, String lastname,String email,String password){
       System.out.println(firstname);
       System.out.println(lastname);
       System.out.println(email);
       System.out.println(password);
    }


    //Fetch data from excel
    @DataProvider
    public Object[][] getData() throws Exception {
        ExcelReaderFactory reader = new ExcelReaderFactory();
        reader.setExcelFile("src/main/java/utils/data.xlsx","Reference Data");
       //Row/Col
        Object[][] data = {{reader.getCellData(1,0),reader.getCellData(1,2),
                            reader.getCellData(1,3),reader.getCellData(0,4)},
                        {reader.getCellData(2,0),reader.getCellData(2,2),
                        reader.getCellData(2,3),reader.getCellData(2,4)}
                            };
        return  data;
    }



}
