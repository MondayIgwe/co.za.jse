package excelFactory;


import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcelReaderFactory {

    private static XSSFWorkbook ExcelWBook;
    private static XSSFSheet ExcelWSheet;
    private static XSSFCell Cell;

    public void setExcelFile(String Path, String SheetName) throws Exception {

        try {
            // Open the Excel file
            File file = new File(Path);
            FileInputStream ExcelFile = new FileInputStream(file);

            // Access the required test data sheet
            ExcelWBook = new XSSFWorkbook(ExcelFile);

            ExcelWSheet = ExcelWBook.getSheet(SheetName);
        } catch (Exception ex) {
            System.out.println("Unable to read excel '"+ex.getMessage()+"'");
        }
        return;
    }

    public String getCellData(int RowNum, int ColNum) throws Exception {

        try {
            Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
            String CellData = Cell.getStringCellValue();
            return CellData;
        } catch (Exception e) {
            return "";
        }
    }

    public Object[][] loopExcel(String excelFile,String sheetName) throws IOException {

        FileInputStream file = null;
        try{
            file = new FileInputStream(excelFile);
            ExcelWBook = new XSSFWorkbook(file);
            ExcelWSheet = ExcelWBook.getSheet(sheetName);

            Object[][] data = new Object[ExcelWSheet.getLastRowNum()][ExcelWSheet.getRow(0).getLastCellNum()];
            for(int row=0;row<ExcelWSheet.getLastRowNum();row++){
                for(int col=0;col<ExcelWSheet.getRow(0).getLastCellNum();col++){
                    data[row][col] =ExcelWSheet.getRow(row + 1).getCell(col).toString();
                   // System.out.println(data[row][col]);
                }
            }
            return data;
        } catch (FileNotFoundException e) {
            throw (e);
        }

    }

    public String getTodaysDate() {
        Date todaysDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return (dateFormat.format(todaysDate));
    }

}
