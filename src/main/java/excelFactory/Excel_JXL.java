package excelFactory;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.read.biff.RowRecord;
import org.junit.Test;

import java.io.*;

public class Excel_JXL {

    Workbook workbook = null;


    @Test
    public void getData() throws IOException, BiffException {
        workbook = Workbook.getWorkbook(new File("src/test/resources/data.xls"));
    try{
        Sheet sheet = workbook.getSheet(0);
        Cell cell1 = sheet.getCell(0, 0);
        System.out.print(cell1.getContents() + ":");    // Test Count + :
        Cell cell2 = sheet.getCell(0, 1);
        System.out.println(cell2.getContents());        // 1

        Cell cell3 = sheet.getCell(1, 0);
        System.out.print(cell3.getContents() + ":");    // Result + :
        Cell cell4 = sheet.getCell(1, 1);
        System.out.println(cell4.getContents());        // Passed

        System.out.print(cell1.getContents() + ":");    // Test Count + :
        cell2 = sheet.getCell(0, 2);
        System.out.println(cell2.getContents());        // 2

        System.out.print(cell3.getContents() + ":");    // Result + :
        cell4 = sheet.getCell(1, 2);
        System.out.println(cell4.getContents());        // Passed 2

    } finally {

        if (workbook != null) {
            workbook.close();
        }
    }
    }
}