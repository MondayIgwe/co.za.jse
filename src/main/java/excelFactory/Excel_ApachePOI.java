package excelFactory;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Excel_ApachePOI {

    static String data = null;
    XSSFWorkbook workbook = null;
    XSSFSheet sheet = null;

    @Test
    public void getData() throws IOException {
        File f = new File("src/test/resources/field.xlsx");
        InputStream in = new FileInputStream(f);
        workbook = new XSSFWorkbook(in);
        sheet = workbook.getSheet("Currencies");
        int row = sheet.getFirstRowNum();
        CellAddress column = sheet.getActiveCell();

        List<Row> rows = new ArrayList<>();
        rows.add(sheet.iterator().next());
        System.out.println(rows);

//        Iterator<Row> it=sheet.iterator();
//        while (it.hasNext()){
//            Row rows = it.next();
//            System.out.println(rows.toString());
//        }
        in.close();
    }
}
