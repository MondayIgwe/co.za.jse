package excelFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Test_ExcelReaderFactory {

    WebDriver driver;
    JavascriptExecutor js;

    @Parameters({"environment-Suite-Level"})
    @BeforeMethod
    public void setUp() throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '"+title+"'");

    }

    @Test(dataProvider = "readExcel")
    public void getData(String firstname,String lastname,String email,String password) throws Exception {
        getDriverFindElement(("//input[@id='firstname' and @name='firstname']")).sendKeys(firstname);
        getDriverFindElement("//input[@id='lastname' and @name='lastname']").sendKeys(lastname);
        getDriverFindElement("//input[@id='email' and @name='email']").sendKeys(email);
        getDriverFindElement("//input[@id='PASSWORD' and @name='PASSWORD']").sendKeys(password);
        Thread.sleep(2000);
        driver.close();
    }


    @org.testng.annotations.DataProvider
    public Object[][] readExcel() throws Exception {
        ExcelReaderFactory reader = new ExcelReaderFactory();
        Object data[][] =  reader.loopExcel("src/main/java/utils/data.xlsx","Reference Data");
       return data;
    }

    public WebElement getDriverFindElement(String xpath){
        WebElement ele = driver.findElement(By.xpath(xpath));
        return ele;
    }

}
