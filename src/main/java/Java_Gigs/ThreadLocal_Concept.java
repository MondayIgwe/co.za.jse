package Java_Gigs;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadLocal_Concept {

    public static void main(String[] args) {
        ThreadLocal_Concept con = new ThreadLocal_Concept();
        System.out.println(con.get());
    }

    private  static final AtomicInteger nextId = new AtomicInteger(7);

    private static final ThreadLocal<Integer> threadId = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return nextId.getAndIncrement();
        }
    };

    public static int get(){
        return threadId.get();
    }


    ///////////////////
    public void getRandomThread(){
        System.out.println("Random Integer: "+ new Random().nextInt());
        System.out.println("Seeded Random Integer: "+ new Random(15).nextInt());
        System.out.println("ThreadLocalRandom "+ThreadLocalRandom.current().nextInt());
        final ThreadLocalRandom random = ThreadLocalRandom.current();
        System.out.println(random.nextDouble(3.5));
    }
}
