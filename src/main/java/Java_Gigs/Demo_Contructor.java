package Java_Gigs;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Demo_Contructor {

    int x;  //Instance Variable
    private List<String> startItems;


    //Getters and Setters
    public List<String> setStartItems(List start) {
        return startItems=start;
    }
    public List<String> getStartItems(){
        return startItems;
    }

    //Constructors
    public Demo_Contructor(int z){
        super();
        x=z;

    }

    public Demo_Contructor(int a,int s){
        this.x=a;
        System.out.println("\n This is the Integer from CONSTRUCTOR 2 \n  "+a);
    }


    //Methods
    public Demo_Contructor test1() {
        final String ALPHANUMERIC = "ABCDWFGHIJKLMNOPQRSTUVWXYZ"+"1234567890";
        System.out.println("return test 1");

        Random random =new Random();
        StringBuilder builder = new StringBuilder();
        int lenght = 13;

        while (lenght-- !=0){
            // generate a random number between
            // 0 to AlphaNumericString variable length
          //  int index = (int)(ALPHANUMERIC.length() * Math.random());
            int index = random.nextInt(ALPHANUMERIC.length()-1);

            builder.append(ALPHANUMERIC.charAt(index));
            System.out.println(builder.toString());
        }
        return new Demo_Contructor(lenght);
    }

        public Demo_Contructor test2(){
            int lenght = 13;
            System.out.println("return test 2");
            return new Demo_Contructor(lenght);
    }

    //ArrayList Concept
    public Demo_Contructor removeDuplicateFromArrayListHashMap(Object[] object){

        HashMap<Object, Integer> map = new HashMap<>();

        for (int i=0;i<object.length;i++){
            if(map.containsKey(object[i])){
                map.remove(object[i]);
            }
            map.put(object[i],i);
        }
        map.forEach((k,v)-> System.out.print("\n"+k + ""));
        return  new Demo_Contructor(9);
    }

    public static long randomIntt(int length) throws Exception {
        long min = 1000000000000L; //13 digits inclusive
        long max = 10000000000000L; //14 digits exclusive
        Random random = new Random();
        long number = min+((long)(random.nextDouble()*(max-min)));
        System.out.println(number);
        return number;
    }

    public static String randomInttt(int length) throws Exception {
        long min = 1000000000000L; //13 digits inclusive
        long max = 10000000000000L; //14 digits exclusive
        Random random = new Random();
        long number = min+((long)(random.nextDouble()*(max-min)));
        String digits = String.valueOf(number);
        System.out.println(digits);
        return digits;
    }
}
