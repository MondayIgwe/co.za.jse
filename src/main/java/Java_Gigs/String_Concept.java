package Java_Gigs;


import java.io.Console;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

public class String_Concept {


    public static void main(String[] args) {
        String_Concept s = new String_Concept();
        s.getString();
    }

    public String getString(){
        final String ALPHANUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder builder = new StringBuilder();

        //Splits
        String parts = "cook,fries";
        String[] part = parts.split(",");
        System.out.println(part[0]);

        char[] data = {'a','b','c'};
        String str = new String(data);
        System.out.println(str);

        //Generate Random Characters
        int randomGenerate = (int)((Math.random()) * ALPHANUMERIC.length());
        builder.append(randomGenerate +str);
        System.out.println("Random generated Numbers are: "+builder.charAt(1));

        //System Concepts
        SecurityManager secure = System.getSecurityManager();
        String systemProperty = System.getProperty("user.dir");
        Properties properties = System.getProperties();
        PrintStream print = System.err;
        InputStream in = System.in;
        Console console = System.console();
        System.out.print(print +" "+ in +" "+console+" "+properties+" "+systemProperty+" "+secure);
        return str;
    }
}
