package Java_Gigs;

import org.junit.Test;

public class Number_Concept {


    Number number = new Integer(200);

    @Test
    public void Run(){
        Object result = get();
        System.out.println(result.toString());
    }
    public Object get(){
        int intV = number.intValue();
        double dVal = number.doubleValue();
        long lVal = number.longValue();
        float fVal = number.floatValue();


        return intV +" "+ dVal +" "+ lVal+" "+fVal;
    }

}
