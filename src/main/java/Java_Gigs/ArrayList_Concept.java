package Java_Gigs;

import gherkin.lexer.Ar;
import org.testng.annotations.Test;

import java.io.File;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

public class ArrayList_Concept extends AbstractList implements List, RandomAccess,Cloneable, Serializable {

    public static void main(String[] args) {
        ArrayList_Concept c = new ArrayList_Concept();
        Object result = c.get(200);
        System.out.println(result);
    }
    
    @Override
    public Object get(int index) {
        return index;
    }

    @Override
    public int size() {
        return 0;
    }

    @Test
    public void listDemo(){
        List<File> f = new ArrayList<>();
        f.add(new File(System.getProperty("user.dir") +"/src/main/java/Xml_Parser_WithJAVA/field_validation.xlsx"));
        System.out.println(f.toString());
    }
}
