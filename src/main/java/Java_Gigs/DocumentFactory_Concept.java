package Java_Gigs;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.openqa.selenium.OutputType;
import org.testng.annotations.Test;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import java.io.*;

public class DocumentFactory_Concept {

    //Create a Document Builder
    private static DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
    private static final String xmlFilePath = System.getProperty("user.dir")+"\\src\\main\\java\\Java_Gigs\\input.xml";

    @Test
    public void docXMLParser() throws ParserConfigurationException, IOException, SAXException {
        try {
            DocumentBuilder builder = dFactory.newDocumentBuilder();

            //Create a document from a file or stream
            Document doc = builder.newDocument();

            //Create root elements
            Element root = doc.createElement("company");
            doc.appendChild(root);

            Element employee = doc.createElement("employee");
            root.appendChild(employee);

            //Set an attribute for staff employee
            Attr attribute = doc.createAttribute("id");
            attribute.setValue("1000");
            employee.setAttributeNode(attribute);

            //Create Firstname Element
            Element firstname = doc.createElement("firstname");
            firstname.appendChild(doc.createTextNode("james"));
            employee.appendChild(firstname);

            //create teh xml file
            //Transform the Dom Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            //Don SOURCE
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            transformer.transform(domSource,streamResult);

        } catch (ParserConfigurationException | TransformerConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        try {
            File inputFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\Java_Gigs\\input.txt");

            DocumentBuilder builder = dFactory.newDocumentBuilder();
            Document doc = builder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element " + doc.getDocumentElement().getNodeName());
            NodeList nlist = doc.getElementsByTagName("student");
            System.out.println("-----------------------------------");

            for (int temp = 0; temp < nlist.getLength(); temp++) {
                Node node = nlist.item(temp);
                System.out.println("\nCurrent Elemengt :" + node.getNodeName());

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    System.out.println("Student roll no : "
                            + eElement.getAttribute("rollno"));
                    System.out.println("First Name : "
                            + eElement
                            .getElementsByTagName("firstname")
                            .item(0)
                            .getTextContent());
                    System.out.println("Last Name : "
                            + eElement
                            .getElementsByTagName("lastname")
                            .item(0)
                            .getTextContent());
                    System.out.println("Nick Name : "
                            + eElement
                            .getElementsByTagName("nickname")
                            .item(0)
                            .getTextContent());
                    System.out.println("Marks : "
                            + eElement
                            .getElementsByTagName("marks")
                            .item(0)
                            .getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}