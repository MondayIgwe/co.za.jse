package Java_Gigs;

import java.awt.*;
import java.awt.event.KeyEvent;
import  java.io.*;

public class Robots_Concept {

    static Robot robot;
    public Robots_Concept(Robot robot){
        super();
        this.robot=robot;
    }

    public static void main(String[] args) throws AWTException, IOException, InterruptedException {
        robot = new Robot();
        robot.delay(2000);
        int delays = robot.getAutoDelay();
        Color color = robot.getPixelColor(20,50);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_1);
        robot.mouseWheel(100);
        robot.setAutoDelay(1000);
        System.out.println(delays+ " "+color);

        new Robots_Concept(robot).typeIntoANotePad();
    }

    public void typeIntoANotePad() throws IOException, InterruptedException {
        String command = "notepad";
        Runtime runtime = Runtime.getRuntime();
        runtime.exec(command);

        robot.keyPress(KeyEvent.VK_H);
        Thread.sleep(1000);
        robot.keyPress(KeyEvent.VK_I);
    }
}
