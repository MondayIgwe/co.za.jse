package Java_Gigs;

import org.junit.Test;
import org.testng.Assert;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

public class File_Concept {

    InputStream in=null;
    InputStream dir=null;
    OutputStream out=null;
    String fileExcel = "src/main/java/Xml_Parser_WithJAVA/field_validation.xlsx";
    String outPutFile = "src/main/java/Xml_Parser_WithJAVA/field.txt";
    private int intVal;
    File output = new File(outPutFile);
    File file = new File(fileExcel);


    @Test
    public void RunTest() throws IOException, InterruptedException {
        fitScrew(getFile());
    }

    public int fitScrew(int x){
        return x;
    }

    public int getFile() throws IOException, InterruptedException {
        if(file.exists()) {

            //Read Input File
            in = new FileInputStream(new File(System.getProperty("user.dir") +
                    File.separator + fileExcel));

            //Read Output Into this File
            out = new FileOutputStream(output);
            out.write(20);


            intVal = in.read();
            System.out.println(intVal +"\n"+file.getAbsolutePath()
            +"\n"+file.getAbsoluteFile()+"\nIs this an absolute path: "+file.isAbsolute()
            +"\nis it a Directory: "+file.isDirectory()+"\nIs it a file: "+file.isFile());


            //Create a temporary file
            if (file.exists()){

                File createdFile = file.createTempFile("temp",".txt",
                        new File(System.getProperty("user.dir")+
                        File.separator+"src/main/java/Xml_Parser_WithJAVA/Created_XMLDOCS"));

                //Check if temp file is created
                System.out.print("File is created: "+createdFile.createNewFile());

                String getFileName = file.getName();
                System.out.print("\nDoes file exist: "+getFileName);

                //Comment to check if file is created
                createdFile.deleteOnExit();

               //Validate/Check if File exist
                Assert.assertEquals("field_validation.xlsx",getFileName);

//                String[] listOfFiles = directory.list();
//                for (int i=0;i<listOfFiles.length;i++){
//                    System.out.println("\n Return Lists of Files: "+listOfFiles.length);
//                }

            }else {
                System.out.println("File Not Created");
            }
        }

        in.close();
        out.flush();
        out.close();
        return intVal;
    }

    @Test
    public void fileDemo() throws IOException {
        File f = new File("");
        Path path = FileSystems.getDefault().getPath("logs","access.log");
        path.endsWith("Logs");
        BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
        String r = reader.readLine();
        URI url = path.toUri();

        Iterator<Path> it = path.iterator();
        while (it.hasNext()){
            System.out.println("Iterator: "+ it.next()+" "+path.toFile());
        }

        if (path.startsWith("logs") == true) {
            Path root = path.getRoot();
            System.out.println(r + "\n" + path.getFileName() + "\n" + path.getNameCount() + "\n" + path.getParent()
                    + "\n" + path.isAbsolute() + "\n" + url+"\n"+root);
            reader.close();
        }
    }


    @Test
    public  void fileTest() throws IOException {
       File ff = new File(System.getProperty("user.dir")+File.pathSeparator+"Logs"+File.pathSeparator+"access.logs");
        System.out.println("RETURN Path using Separator: "+ff.getPath()+"\nRETURN Path NAME: "+ff.getName()
                +"\nRETURN Parent Path: "+ff.getParentFile()+"\nRETURN PATH SPACE: "+ff.getFreeSpace());

       File f = new File("Logs","access.log");
        System.out.println("Return Parents & Child: "+f.getAbsolutePath());

        if(f.canExecute()){
            f.canRead();
            f.canWrite();
            int compare = f.compareTo(ff);
            System.out.println("Comparing File: "+compare);

            if(ff.exists()){
               File createdFile =  File.createTempFile("temp",".txt",new File(System.getProperty("user.dir")+File.pathSeparator+"Logs"));
                createdFile.getAbsolutePath();
                createdFile.getParent();

                String[] files = ff.list();
                for(String fileList:files){
                    System.out.println(fileList);
                }
            }
        }
    }
}
