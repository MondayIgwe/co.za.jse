package Java_Gigs.AdvanceJAVA;

import org.junit.Test;

public class Method_Concepts {

    @Test
    public void runTest(){
        //methodExplain(5);
        recursivelyPrint(5);
    }

    public int methodExplain(int i){
        int x=i;
        methodExplain(x);
        return x++;
    }

    public static void recursivelyPrint(int num) {
        System.out.println("Number is: " + num);

        if(num == 0)
            return;
        else
        {
            num +=1;
            recursivelyPrint(num);
        }
    }

}
