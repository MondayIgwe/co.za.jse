package Java_Gigs.AdvanceJAVA;

import Java_Gigs.Demo_Contructor;

import java.io.File;

public class Fundamental extends Demo_Contructor{

    char c; //Primitive type
    static Demo_Contructor demo=null; //Object/Reference Type
    File image=null;  //Object/Reference Type


  /////////Constructor Overloading=========================
    public Fundamental(){
        super(3,5);
        c = 'L';
        image= new File("The File is Done by the Constructor:  "+System.getProperty("user.dir")+"/src/main/java/TakeScreenshot/Screenshots/image1588233853080.jpg");
        System.out.println(image.getAbsolutePath());
        demo = new Demo_Contructor(4,6).test1();
        new Demo_Contructor(2,3).test2();
    }

    public Fundamental(Demo_Contructor demo){
        super(500,400);
        Fundamental.demo = demo;
        System.out.println("SECOND Fundamental Initialized..");
    }


    public static void main(String[] args) {
        //Primitive Data Types
        byte b = 10; //8bits
        short s = 10; //12bits
        int i = 10; //32
        long l = 10; //64bits
        float f = 10; //16bits
        double d = 10; //32 bits
        char c = 'A';
        boolean boo = false; //default is false

        //Java Class/Objects Types
        Byte myByte = new Byte("10");
        Short myShort = new Short(s);
        Integer myInteger = Integer.valueOf(i);
        Float myFloat = Float.valueOf(f);
        String strr = "tooth";
        Float ourFloat = new Float(0.1);
        Double myDouble;
        Character myCharacter;
        Boolean myBoolaen;
        String str = String.valueOf(i);

        System.out.println(myByte + "\n" + myShort + "\n" + myInteger.intValue() + "\n" + myFloat + "\n" + str + "\n" + ourFloat);
        System.out.println();

        String firefox = "path/to/Firefox";

        switch (firefox) {
            case "path/to/Firefox":
                System.out.println("Firefox");
                break;
            case "path/f":
                System.out.println("Chrome");
                break;
            default:
                System.out.println("Not Found!");
                break;
        }


        ///////////////////////StringBuffer & StringBuilder//////////////////
        String attachSTRING = "i LOVE IT ";
        StringBuffer buffer = new StringBuffer("First, Java is Cool, ");
        StringBuilder builder = new StringBuilder("Second, Java is cool, ");

        buffer.append(attachSTRING);
        builder.append(attachSTRING);

        //Loop
        for (int z=0;z<2;z++){
            StringBuffer result = buffer.append(", --and the index is: "+z);
            System.out.println("The Loop is: '"+ result + "' ");
        }
        System.out.println("\n"+buffer+"\n"+builder);

        //////////////Instantiate Class ==> Object Type/Reference
        Fundamental fundamental = new Fundamental();
        Fundamental fundamentalOverLoad = new Fundamental(demo.test2());

        fundamental.initializeMethod();
        System.out.println("divide Object/Reference");
        fundamentalOverLoad.initializeMethod();

        //////Pass By Value//////////////////////////////////////////
        double passByValue = 2.0;
        fundamental.changeMe(passByValue);
        System.out.println("\n"+passByValue);
        System.out.println();

        //////////////Pass By Reference//////////////////////////////////////
        Car BMW = new Car();
        Car FORD = new Car();

        BMW.setSpeed(23.00);
        BMW.changeCarParameter(BMW);
        System.out.println("\nPass By Reference SPEED  from the Method:  "+BMW.getSpeed());

        FORD.setSpeed(3000.00);
        FORD.changeCar(FORD);
        System.out.println("\nPass By Value SPEED  from the Method:  "+FORD.getSpeed());
    }

    /////////////////////Arrays///////////////////////////////////////////////

    int[] arr = new int[12]; //Primitive Type Array
    String[] strrr = new String[12];//Object/Reference Type Array

    //Initialize Types (Primitive or Object Types/Reference)
    int[] initInSide;

    public void initializeMethod(){
        initInSide = new int[2];

        int[] myDou = {1,2,3};
        for(int i=0;i<myDou.length;i++){
            Object obj = Object.class;
            System.out.println("Return Double as Integer: '"+myDou[i]+'"');
            System.out.println("The class of the object type is: "+obj + "\n"+Object.class);

        }
    }

    ////////////////Pass By Value
    public void changeMe(double d){
        d = 34.0;
    }


    //Pojo class/////Pass By Reference
    static class Car{

        private double speed;

      public double getSpeed() {
          return this.speed;
      }

        public void setSpeed(double speed) {
            this.speed = speed;
        }

        public Car changeCarParameter(Car c){
          //Change car speed outside this method
            c.setSpeed(900.0);
            return new Car();
        }

        public Car changeCar(Car c){
          Car ford = new Car();
          ford.setSpeed(1000.00);
          //does not affect car object outside this method
           return c=ford;
        }

    }

}
