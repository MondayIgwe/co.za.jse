package Java_Gigs.AdvanceJAVA;

import org.junit.Test;

public class SuperClass {

    Object obj;

    public SuperClass(){
        obj = new Object();
        System.out.println(obj.getClass());
    }

    @Test
    public void runTest(){
        methodVarArgsInt();
        System.out.println();

        //pass any number of parameters to the argument
        methodVarArgs(700, 9, 76, 98);//You can pass any number of parameters to the argument
        System.out.println();
        methodVarAgrsStrings("pepsi"+","+"fanta"+","+"Milk");
        System.out.println();
        calcAverageVarArgs(2);
    }

    public int methodVarArgs(int...a){ //You can pass any number of parameters to the argument
        int total=0;
        for (int i=0;i<a.length;i++){
            total +=a[i];
        }
        System.out.println("Total is: "+total);
        return total;
    }


    public int methodVarArgsInt(){
        int[] str = {1,2,3,4,5,6,7,8,9};
        int result=0;
        for (int i=0;i<str.length;i++){
            result +=str[i];
        }
        System.out.println(result);
        return result;
    }

    public void methodVarAgrsStrings(String...str){//pass any number of parameters to the argument
        StringBuilder builder = new StringBuilder();
        for (String s : str){
            builder.append(s);
        }
        System.out.println(builder.toString());
    }

    public double calcAverageVarArgs(double... numberOfUsers){
        double totalUsers=0.0;
        for (double x:numberOfUsers){
            totalUsers +=x;
        }
        System.out.println(totalUsers);
        return totalUsers / numberOfUsers.length;
    }

}
