package Java_Gigs.AdvanceJAVA;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Monday
 * 
 */
public class AutoBoxing {

    @Test
    public void RunTest(){
      autoBox();

      List<Integer> list = new ArrayList<>();
      list.add(33);
      list.add(500);
      sumEven(list);
      collectionDeo();
    }

    public void autoBox() {
        List<Integer> listIntegers = new ArrayList<>();
        for (int i=1;i<10;i++){
            listIntegers.add(Integer.valueOf(i));
            int firstNumber = listIntegers.get(0);
            System.out.println(firstNumber);
        }


    }

    public int sumEven(List<Integer> listIntegers){
        int sum =0;
        for(Integer i:listIntegers){
            if (i % 2 == 0){
                sum +=i;
            }
            System.out.println(sum);
        }
        return sum;
    }

    ///////////////////Collection Interface///////////////
    Collection<String> nameList;
    Collection<StringBuffer> nameBuffer;
    public Collection<String> collectionDeo(){
        nameList = new ArrayList<>();
        nameBuffer = new ArrayList<>();

        nameList.add("John");
        nameList.add("june");
        nameList.add("goo");

        for (String names:nameList){
            System.out.println(names);
        }

        //Collection with StringBUFFER
        nameBuffer.add(new StringBuffer("Jerry"));
        nameBuffer.add(new StringBuffer("brown"));
        nameBuffer.add(new StringBuffer("moon"));

        for (StringBuffer buffer:nameBuffer){
            buffer.reverse();
            System.out.println(buffer);
        }

        return nameList;
    }



}
