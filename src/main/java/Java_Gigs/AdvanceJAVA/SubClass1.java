package Java_Gigs.AdvanceJAVA;

import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;

public class SubClass1 extends SuperClass {

    private int d=0;
    private  int count=2;
    private int numericIncreas=0;

    WatchService watchService =null;


    //Static Initializer
    static {
        System.out.println("Static Initializer...........");
    }

    //Instances Initializer
    {
        System.out.println("\nInstance Initializer...........");
    }

    public SubClass1(){
        super();
       for(int i=0;i<count;i++){
            d= ++numericIncreas;
           System.out.println("Increased by: 'size:"+d+"'   index:"+ i);
       }

    }

    public void draw(){
        System.out.println("Super Class drawing");

    }

    public void file() throws IOException, InterruptedException {
        watchService = FileSystems.getDefault().newWatchService();
        watchService.take();
    }

    @Test(testName = "Wrapper Classes",description = "Describing the Test")
    public void wrapperClasses(){
        int parseInt = Integer.parseInt("3"); ///the value must be Number NOT  a letter inside the string
        Integer wrapper = Integer.valueOf(200);
        int primitive = wrapper.intValue();
        System.out.println(parseInt +", "+wrapper+" ,"+primitive);
    }
}
