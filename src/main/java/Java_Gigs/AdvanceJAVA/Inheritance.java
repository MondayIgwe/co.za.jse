package Java_Gigs.AdvanceJAVA;

import org.junit.Test;

public class Inheritance  {

    SubClass1 subClass1;
    SubClass2 subClass2;

    final String BROWSER ="FIREFOX";


    @Test
    public void runTest() {
        switch (BROWSER) {
            case "CHROME":
                subClass1 = new SubClass1();
                subClass1.draw();
                System.out.println("Draw from Subclass 1");
                break;
            case "FIREFOX":
                subClass2 = new SubClass2("ko");
                SuperClass printt = subClass2.write();
                System.out.println("Write from Subclass 2 =="+printt);
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
