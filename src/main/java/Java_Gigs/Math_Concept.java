package Java_Gigs;

import org.testng.annotations.Test;

public class Math_Concept {
    @Test
    public void testMath(){
        demoMath();
    }

    public void demoMath(){
        double e = Math.E;
        double pi = Math.PI;
        int a = (int)Math.abs(200.00);
        int add = Math.addExact(28,50);
        long increase = Math.incrementExact(200000l);

        boolean result = false;
        if((Math.multiplyExact(2,2) == 4)){
            result = true;
            System.out.println(result);
        }
        System.out.println(e +"\n"+a+"\n"+pi+"\n" +
                "\n"+add+"\n"+increase);
    }
}
