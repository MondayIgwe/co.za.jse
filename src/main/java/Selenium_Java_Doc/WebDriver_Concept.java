package Selenium_Java_Doc;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.Dimension;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class WebDriver_Concept {
    WebDriver driver=null;

    public void webdriverDemo() throws MalformedURLException {
        WebDriverManager.chromedriver().timeout(5000).setup();
        ChromeOptions options = new ChromeOptions();
        options.addExtensions(new File("/path/to/extension.crx"));
        options.setCapability("",new Integer(2));
        driver = new ChromeDriver();
        driver.get("website");

        Dimension dimension = new Dimension(200,400);
        dimension.getWidth();
        dimension.getHeight();

        driver.findElement(By.xpath(""));

        Set<String> windows= driver.getWindowHandles();
        for (int i=0;i<windows.size();i++){
            Iterator it = windows.iterator();
            if (it.hasNext()){
                System.out.println(windows);
                driver.switchTo().frame("frameName");
            }
        }


        //For RemoteWebdriver
      //  DesiredCapabilities cap = new DesiredCapabilities.chrome();
      //  cap.setCapability(ChromeOptions.CAPABILITY,options);
      //  RemoteWebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);
    }
}
