package Selenium_Java_Doc;

import TakeScreenshot.TakeScreenshot_Concept;
import com.paulhammant.ngwebdriver.*;
import deleteAndcopyFiles.CopyFiles;
import deleteAndcopyFiles.DeleteFiles;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class NGWebdriver_Concept {
/**
 * @author monday
 * @version 1.0
 * Angular JS is a opensource web application framework based on Java-script.
 * Any Angular JS implemented web page would have additional custom tag attributes like
 * ng-app, ng-model, ng-bind, ng-repeat etc.............
 * For people who are on java version of Selenium & do not want to switch to Protractor,
 * there is an option of enhancing the Selenium ability to locate angular tags by using NgWebDriver.
 */


    private  WebDriver driver;
    private NgWebDriver ngWebDriver;
    static String empName = "TestNet Technologies";
    boolean checkName = !false;
    File SOURCEDIRECTORY;
    File TARGETDIRECTORY;

    public NGWebdriver_Concept(){
        SOURCEDIRECTORY = new File(System.getProperty("user.dir")+ File.separator
                + "/src/main/java/TakeScreenshot/Screenshots");
        TARGETDIRECTORY = new File(System.getProperty("user.dir")+ File.separator
                + "/src/main/java/deleteFiles/deleteThisFiles");
    }
    @BeforeMethod
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        setDriver(driver);
        driver = new ChromeDriver();

        //Sample AngularJs Webpage
        getDriver().get("https://hello-angularjs.appspot.com/sorttablecolumn");
        getDriver().manage().window().maximize();

        //Creating an instance of NgWebDriver
        ngWebDriver = new NgWebDriver((JavascriptExecutor)driver);

    }

    @Test(description = "NGWebdriver Angular Protractor with Java Selenium ")
    public void RunTest() throws InterruptedException, IOException {
        SoftAssert softAssert = new SoftAssert();
        int count =1;
        int randomNumber = ((int)((Math.random() * 1)));

            getDriver().findElement(ByAngular.model("name")).sendKeys(empName);
            getDriver().findElement(ByAngular.model("employees")).sendKeys("Automation Tester");
            getDriver().findElement(ByAngular.model("headoffice")).sendKeys("SANDTON");
            getDriver().findElement(ByAngularButtonText.xpath("//*[@value='Submit' and @type='submit']")).click();
            TakeScreenshot_Concept.getScreenShots(getDriver());
            Thread.sleep(2000);

        //Loop through Table to validate created instances
        WebElement table = getDriver().findElement(By.cssSelector("body > div > div:nth-child(6) > table > tbody > tr > td:nth-child(1) > table > tbody"));
        List<WebElement> rows = table.findElements(By.tagName("tr"));
        System.out.println("get number of rows: " + rows.size());

        while (rows != null) {
            for (int i = 0; i < rows.size(); i++) {
                String empNames = getDriver().findElement(ByAngular.repeater("company in companies").row(i).column("name")).getText();
                System.out.println("Text found: " + empNames.trim());

                if (empNames.equalsIgnoreCase(empName)) {
                    System.out.println("New company has been added: '" + empName + "'");
                    System.out.println("Match Found and Removed  '"+empName+"'  from registered Companies");
                    checkName = rows.contains(empName);
                    softAssert.assertEquals(rows.contains(empName), empName);
                    TakeScreenshot_Concept.getScreenShots(getDriver());
                    driver.findElement(ByAngular.repeater("company in companies").row(i)).findElement(ByAngular.buttonText("Remove")).click();

                    //Take ScreenShots
                    TakeScreenshot_Concept.getScreenShots(getDriver());
                    Thread.sleep(2000);

                    //Copy and Delete screenshots
                    CopyFiles.copyFile(SOURCEDIRECTORY,TARGETDIRECTORY);
                    DeleteFiles.deleteDirectory(TARGETDIRECTORY);
                    break;
                }else {
                    boolean getBooleanValue = checkName=rows.contains(empName);
                    System.out.println("Name '"+empNames+"' No Match!  " + getBooleanValue);
                }
            }
            break;
        }
    }

    public WebDriver getDriver(){
        return driver;
    }

    public void setDriver(WebDriver driver){
        this.driver=driver;
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        try{
            Thread.sleep(2000);
        }finally {
            getDriver().close();
        }
    }
}
