package deleteAndcopyFiles;

import java.io.File;

public class DeleteFiles {

    public static boolean deleteDirectory(File dir) {
        for (File file : dir.listFiles()) {
            if (file.isDirectory())
                deleteDirectory(file);
            file.delete();
            System.out.println("Files deleted" + file.getName());
        }
        return true;
    }
}

