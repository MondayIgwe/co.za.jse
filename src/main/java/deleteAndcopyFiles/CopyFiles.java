package deleteAndcopyFiles;

import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;

public class CopyFiles {

    public static boolean copyFile(File source,File target) throws IOException {
        //copy source to target using Files Class
        FileUtils.copyDirectory(source,target);
        return true;
    }
}
