package TestNG_Demo;

import environments.Base_Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

@Listeners(TestNG_Listeners.class)
public class Test_3 extends Base_Setup {

    @Parameters({"environment-Suite-Level"})
    @BeforeMethod
    public void setUp() throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '"+title+"'");

    }

    @Test(dataProvider = "testData")
    public void getData(String firstname,String lastname,String email,String password) throws Exception {
            Thread.sleep(2000);
            getDriverFindElement(("//input[@id='firstname' and @name='firstname']")).sendKeys(firstname);
            getDriverFindElement("//input[@id='lastname' and @name='lastname']").sendKeys(lastname);
            getDriverFindElement("//input[@id='email' and @name='email']").sendKeys(email);
            getDriverFindElement("//input[@id='PASSWORD' and @name='PASSWORD']").sendKeys(password);
            driver.close();
        }


    @DataProvider(name = "testData")
    public Object[][] getData(){
        //Row/Col
        Object[][] data = new Object[2][4];

        //Row 1 & Four Columns
        data[0][0] = "user1";
        data[0][1] = "joo";
        data[0][2] = "user1@yahoo.com";
        data[0][3] = "user12345";

        //Row 2  & Four Columns
        data[1][0] = "user2";
        data[1][1] = "moon";
        data[1][2] = "user2@gmail.com";
        data[1][3] = "user09875";
        return  data;
    }

    public WebElement getDriverFindElement(String xpath){
           WebElement ele = driver.findElement(By.xpath(xpath));
        return ele;
    }
}
