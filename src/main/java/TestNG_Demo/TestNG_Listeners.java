package TestNG_Demo;


import ExtentReportListener.GenerateExtentReport;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNG_Listeners implements ITestListener {
    GenerateExtentReport generateExtentReport = new GenerateExtentReport();

    private  ThreadLocal<ExtentTest> extentTestThreadLocal = new ThreadLocal<ExtentTest>();

    //Before class execution
    public void onStart(ITestContext arg){
        generateExtentReport.startReport();
        System.out.println("Test Started  "+arg.getName());
        Assert.assertEquals("noo","noo");
    }

    //After Class ends
    public void onFinish(ITestContext arg){
        generateExtentReport.endReport();
        System.out.println("Test Finish  "+arg.getName());
        Assert.assertEquals("noo","noo");
    }

    //Before Test Start
    public void onTestStart(ITestResult result){
        System.out.println("Test Start  "+result.getMethod().getDescription()+"'");
        Assert.assertEquals("noo","noo");
    }

    public void onTestSuccess(ITestResult result){
        generateExtentReport.passTest();
        System.out.println("Test Passed "+result.getMethod().getDescription());
        Assert.assertEquals("noo","noo");
        generateExtentReport.getResult(result);
    }

    public void onTestSkipped(ITestResult result){
        generateExtentReport.failTest();
        System.out.println("Skipped test  "+result.getMethod().getDescription());
        System.out.println("Skipped test  "+result.getMethod().getDescription());
        generateExtentReport.getResult(result);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult arg){
        System.out.println("Fail or Success test  "+arg.getName());
    }
}
