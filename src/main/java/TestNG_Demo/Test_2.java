package TestNG_Demo;

import environments.Base_Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Test_2 extends Base_Setup {


    @Parameters({"environment-Suite-Level"})
    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '"+title+"'");

    }

    @Test(dataProvider = "data")
    public void getData(String firstname,String lastname,String email,String password) throws Exception {
            getDriverFindElement(("//input[@id='firstname' and @name='firstname']")).sendKeys(firstname);
            getDriverFindElement("//input[@id='lastname' and @name='lastname']").sendKeys(lastname);
            getDriverFindElement("//input[@id='email' and @name='email']").sendKeys(email);
            getDriverFindElement("//input[@id='PASSWORD' and @name='PASSWORD']").sendKeys(password);
            Thread.sleep(2000);
            driver.close();
    }

    @DataProvider(name = "data")
    public Object[][] getData(){
        //Row/Col
        Object[][] data = {
                            {"testing1", "test1", "first@gmail.com", "moredata1"}, //Row 1
                            {"testing2", "test2", "second@gmail.com", "moredata1"}, //Row 2
                            {"testing3", "test3", "second@gmail.com", "moredata1"} //Row 3
                         };
        return  data;
    }

    public WebElement getDriverFindElement(String xpath){
        WebElement ele = driver.findElement(By.xpath(xpath));
        return ele;
    }
}
