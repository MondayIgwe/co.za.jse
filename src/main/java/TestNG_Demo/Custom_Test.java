package TestNG_Demo;

import org.testng.annotations.Parameters;

public class Custom_Test {

    @org.testng.annotations.Test(dataProvider = "instance",dataProviderClass = CustomDataProvider.class)
    public void getInstance(String data1, String data2){
        System.out.println(data1 +"\n"+ data2);
    }

}
