package TestNG_Demo;

import org.testng.annotations.DataProvider;

public class CustomDataProvider {

    @DataProvider(name="instance")
    public Object[][] getInstance(){
        Object[][] data = new Object[2][2];

        //Row 1
        data[0][0] = "Row zero, First Column";
        data[0][1] = "Row zero, Second Column";

        //Row 2
        data[1][0] = "Row one,First Column";
        data[1][1] = "Row one, Second Column";

        return data;
    }
}
