package JDBC;

import java.sql.*;

public class ConnectToDataBase {

    private Connection con;
    private Statement st;
    private ResultSet rs;

    final String DATABASE_URL =  "jdbc:mysql://localhost/student";
    final String USERNAME = "root";
    final  String PASSWORD = "";

    public ConnectToDataBase connection() throws Exception {
        try{
            //load the mysql driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Database connected");

            //get the connection
            con = DriverManager.getConnection(DATABASE_URL,USERNAME,PASSWORD);

            //create a statement
            st = con.createStatement();

            getData();
        }catch (Exception e){
            throw (e);
        }
        return  new ConnectToDataBase();

    }

    //Get data from RecordSet
    public ConnectToDataBase getData() throws Exception {
        try{
            String query = "SELECT * FROM admin";
            rs = st.executeQuery(query);
            System.out.println("Records of the table");
            while (rs.next()){
                String id = rs.getString("id");
                String mail = rs.getString("mail");
                String userName = rs.getString("username");
                String password = rs.getString("password");
                System.out.println("ID: "+id+ "Mail: "+mail+ "USERNAME: "+userName + "PASSWORD: "+password);
            }
        }catch (Exception e){
            throw new Exception("Error"+e.getMessage());
        }
        return new ConnectToDataBase();
    }
}
