package JsonToJava;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WriteJsonToJava {


    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        Pojo_Class pojo = createStaff();

        try{
            //Java object to json file
            mapper.writeValue(new File("src/main/java/JsonToJava/Obj.json"), pojo);

            //java object to jason
           String jsonObj = mapper.writeValueAsString(pojo);
            System.out.println(jsonObj);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private static Pojo_Class createStaff(){
        Pojo_Class pojo_class = new Pojo_Class();

        pojo_class.setName("joo");
        pojo_class.setAge(90);
        pojo_class.setPosition(new String[]{"Automation","Manual"});
        Map<String, BigDecimal> salary = new HashMap(){{
            put("2010",100010000);
            put("2020",1000000000);
        }};
        pojo_class.setSalary(salary);
        pojo_class.setSkill(Arrays.asList("java","c#","python"));

        return pojo_class;
    }
}
