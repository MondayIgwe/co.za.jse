package JsonToJava;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Pojo_Class {

    //POJO

    private String name;
    private int age;
    private String[] position;
    private List<String> skill;
    private Map<String, BigDecimal> salary;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getPosition() {
        return position;
    }

    public void setSalary(Map<String, BigDecimal> salary) {
        this.salary = salary;
    }

    public void setPosition(String[] position) {
        this.position = position;
    }

    public Map<String, BigDecimal> getSalary() {
        return salary;
    }

    public void setSkill(List<String> skill) {
        this.skill = skill;
    }

    public List<String> getSkill() {
        return skill;
    }
}
