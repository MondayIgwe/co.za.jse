package Xml_Parser_WithJAVA;


import javax.xml.parsers.*;

import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import org.junit.Test;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;


public class java_dom_xmlParser {

    public static void main(String[] args) throws Exception {
        java_dom_xmlParser test = new java_dom_xmlParser();
        //test.Dom();
        System.out.println();
        //test.transformerFactory();
        //test.creatXml();
        test.createXMLforInstan("Firm","svc_tbc60.xml");
    }

    public static void Dom() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();

            //Root Element
            Document doc = parser.parse(System.getProperty("user.dir")+"/src/main/java/Xml_Parser_WithJAVA/data.xml");
            Element root = doc.getDocumentElement();

            NodeList children = root.getElementsByTagName("question");
            System.out.println("The children are: '" + children + "'");

            for (int i = 0; i < children.getLength(); i++) {
                Node nodesKids = children.item(i);
                //String va = ((Element)nodesKids).getAttributes();

                if (nodesKids.getNodeType() == Node.ELEMENT_NODE) {
                    System.out.println(nodesKids.getFirstChild().getTextContent());
                }
            }

            NodeList nodeTag = doc.getElementsByTagName("question");
            System.out.println("The element  " + (Element) nodeTag);

            Element id = doc.getElementById("id");
            System.out.println(id.getFirstChild().getNodeValue());

            //Display XML on the screen
            transformerFactory();
        } catch (Exception e) {
            System.out.println();
        }

    }


    public static void transformerFactory() throws ParserConfigurationException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document doc = parser.parse(System.getProperty("user.dir")+"/src/main/java/Xml_Parser_WithJAVA/data.xml");
            Element root = doc.getDocumentElement();

            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer =tFactory.newTransformer();

            DOMSource source = new DOMSource(root);
            StreamResult result = new StreamResult(System.out);
            transformer.transform(source,result);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public void creatXml() {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            //Root Element
            Document doc = documentBuilder.newDocument();

            Element rootElement = doc.createElement("millonare");
            doc.appendChild(rootElement);

            //After thr root node, follows this node
            Element elementFirst = doc.createElement("info");
            rootElement.appendChild(elementFirst);

            Element elementSecond = doc.createElement("info");
            rootElement.appendChild(elementSecond);

            //Set Attribute to element node
            Attr attr = doc.createAttribute("id");
            attr.setValue("details");

            elementFirst.setAttributeNode(attr);

            //=============First Element================
            //FirstName
            Element element1 = doc.createElement("firstname");
            element1.appendChild(doc.createTextNode("igwe"));
            elementFirst.appendChild(element1);

            //LastName
            Element element2 = doc.createElement("lastname");
            element2.appendChild(doc.createTextNode("monday"));
            elementFirst.appendChild(element2);

            //salary
            Element element3 = doc.createElement("salary");
            element3.appendChild(doc.createTextNode("100,00000"));
            elementFirst.appendChild(element3);

            //=============Second Element================
            //FirstName
            Element element11 = doc.createElement("firstname");
            element11.appendChild(doc.createTextNode("igwe"));
            elementSecond.appendChild(element1);

            //LastName
            Element element22 = doc.createElement("lastname");
            element22.appendChild(doc.createTextNode("monday"));
            elementSecond.appendChild(element2);

            //salary
            Element element33 = doc.createElement("salary");
            element33.appendChild(doc.createTextNode("100,00000"));
            elementSecond.appendChild(element3);

            //Write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            //DOM source with accept the document
            DOMSource source = new DOMSource(doc);

            // Output to console to file
            StreamResult result = new StreamResult(new File(System.getProperty("user.dir") + "/src/main/java/Xml_Parser_WithJAVA/file.xml"));

            // Output to console for testing
            Result onConsoleLog = new StreamResult(System.out);
            System.out.println("_____________XML STARTED____________");
            System.out.println("");

            //Beautify the format of the excel
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(source, result);
            transformer.transform(source, onConsoleLog);
            System.out.println("A new file with the name '" + onConsoleLog + "'");
            System.out.println("");
            System.out.println("_____________XML ENDED____________");


            System.out.println("-------------READ THE XML--------");
            Document docRead = documentBuilder.parse(new File(System.getProperty("user.dir")+"/src/main/java/Xml_Parser_WithJAVA/file.xml"));
            docRead.getDocumentElement().normalize();
            System.out.println("Root element: " +docRead.getDocumentElement().getNodeName());

           NodeList children = docRead.getElementsByTagName("info");

           for(int i=0;i<children.getLength();i++){
               Node kids = children.item(i);
               System.out.println(kids.getTextContent());
               if(kids.getNodeType()==Node.ELEMENT_NODE){
                   Element ele = (Element) kids;
                   System.out.println("Info id: "+ele.getAttribute("id"));
               }
           }

            //Get the root ATTRIBUTE
//            NodeList rootNode = root.getElementsByTagName("millonare");
//            System.out.println(rootNode.item(1));

        } catch (ParserConfigurationException | TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    ////////////////EXCEL SQL HELPER METHO
    public void createXMLforInstan(String Instance,String fileName) throws Exception {
       // = "Firm";
        // = "Testing";
        try
        {
            //Read the Excel
            Fillo fillo=new Fillo();
            Connection connection = fillo.getConnection(System.getProperty("user.dir") + "/src/main/java/Xml_Parser_WithJAVA/field_validation.xlsx");
            String strQuery="SELECT * FROM "+Instance+" where InUse='Yes' and XMLUse='Yes'";
            Recordset recordset=connection.executeQuery(strQuery);
            int rowCount = recordset.getCount();

            //StART TRANSFORMING the RExcel to XML
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            // File root element
            Element rootElement = doc.createElement("FILE");
            doc.appendChild(rootElement);
            // Instance root element
            Element InstanceRoot = doc.createElement("INSTANCE");
            rootElement.appendChild(InstanceRoot);

            int length;
            // Field elements
            for(int i=0; i<rowCount; i++)
            {

                recordset.moveNext();

                Element fieldName = doc.createElement(recordset.getField("XML_Field"));
                if (recordset.getField("Data_Type").equalsIgnoreCase("String")){
                    length = Integer.valueOf(recordset.getField("Length"));
                    fieldName.appendChild(doc.createTextNode(randomString(length)));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Enum")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Integer")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("SourcedList")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("StringFixed")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Double")){
                    String Default = recordset.getField("XML_Possible_Values");
                    fieldName.appendChild(doc.createTextNode(Default));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Date")){
                    String Default = recordset.getField("XML_Possible_Values");
                    fieldName.appendChild(doc.createTextNode(Default));
                }
                InstanceRoot.appendChild(fieldName);
            }

            recordset.close();
            connection.close();

            Element fieldName = doc.createElement("Action");
            fieldName.appendChild(doc.createTextNode("0"));
            InstanceRoot.appendChild(fieldName);

            // Write the content into XML file
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(System.getProperty("user.dir") + "/src/main/java/Xml_Parser_WithJAVA/Created_XMLDOCS/"+fileName));
            Result dest = new StreamResult(System.out);

            //Transform the xml
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // Beautify the format of the resulted XML
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            transformer.transform(source, result);

            System.out.println("");
            System.out.println("_____________XML____________");

            transformer.transform(source, dest);

            System.out.println("_____________XML_END____________");
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            throw (ex);
        }
    }

    public String randomString(int length) throws Exception {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        //System.out.println( builder.toString());
        return builder.toString();
    }

}