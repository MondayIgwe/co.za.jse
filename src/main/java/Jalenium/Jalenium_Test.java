package Jalenium;

import com.github.lkkushan101.jalenium.*;
import org.testng.annotations.Test;

public class Jalenium_Test {

    @Test
    public void testLogin() throws InterruptedException
    {
        new Jalenium();

        Jalenium.Go( "http://demo.guru99.com/V4/");
        Jalenium.InteligentType("uid", "mngr257744");
        Jalenium.InteligentType("password", "dezanEb");
        Jalenium.InteligentClick("LOGIN");
        Jalenium.AssertElementText("xpath=//tr[@class='heading3']/td", "Manger Id : mngr257744");
        Jalenium.CloseBrowser();
    }
}
