package environments;

import commonLibs.CommonLibs;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base_Setup {

    public WebDriver driver;
    public JavascriptExecutor js;

    @Before
    public void getDriver() throws Exception {
        try{
            System.out.println("Initializing Driver for AUT");
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            SetupConfig.getDetails();
            driver.get(SetupConfig.URL);
            driver.manage().window().maximize();
            //driver.manage().deleteAllCookies();
            js = ((JavascriptExecutor) driver);
            String title = js.executeScript("return document.title;").toString();
            System.out.println("The page title is '"+title+"'");
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @After
    public void tearDown(){
        CommonLibs commonLibs = new CommonLibs(driver);
        try{
            commonLibs.threadSleep(2000);
            driver.quit();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

}
