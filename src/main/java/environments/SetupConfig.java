package environments;


import java.io.FileReader;
import java.util.Properties;

public class SetupConfig
{

    public static Properties propObj = null;

    static FileReader inputStreamObj = null;
    public static String URL = null;
    public static  String navigate = null;
    public static  String username = null;
    public static String password = null;

    //STATIC METHOD
    public static void getDetails()throws Exception{
        try
        {
            propObj = new Properties();
            inputStreamObj = new FileReader(System.getProperty("user.dir")+"/src/test/resources/application.properties");

            //LOAD AND READ THE PROPERTIES FILE FROM ENVIRONMENTS
            propObj.load(inputStreamObj);
            URL = propObj.getProperty("url");
            navigate = propObj.getProperty("navigate");
            username = propObj.getProperty("username");
            password = propObj.getProperty("password");
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if (inputStreamObj !=null){
                try {
                    inputStreamObj.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

}
