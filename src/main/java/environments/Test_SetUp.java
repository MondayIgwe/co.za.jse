package environments;

import commonLibs.CommonLibs;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.TestUtils;

import java.io.IOException;

public class Test_SetUp {

    public static WebDriver driver;
    public static JavascriptExecutor js;

    @BeforeSuite
    public static void setUp(String Url) throws Exception {
        try{
            System.out.println("Initializing Driver for AUT");
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.get(Url);
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            js = ((JavascriptExecutor) driver);
            String title = js.executeScript("return document.title;").toString();
            System.out.println("The page title is '"+title+"'");
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }



    @AfterClass
    public static void tearDown(ITestResult result) throws IOException {
        TestUtils testUtils = null;
        CommonLibs commonLibs = new CommonLibs(driver);
        if(result.getStatus() == ITestResult.FAILURE){
            testUtils.takeScreenshotAtEndOfTest(result.getName().trim(),driver);
            commonLibs.threadSleep(2000);
            driver.quit();
        }
    }
}
