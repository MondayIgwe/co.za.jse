package commonLibs;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class CommonLibs {

    WebDriver driver;

    public CommonLibs(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    public void highLighterMethod(  WebDriver driver,WebElement element){
        ((JavascriptExecutor)driver)
                .executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
    }

    public void JavaScriptClickElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
    }

    public void scrollIntoElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true)", element);
    }

    public void generateAlert(WebDriver driver,String message){
        try{
            ((JavascriptExecutor)driver).executeAsyncScript("alert('"+message+"')");
            Thread.sleep(1000);
        }catch ( Exception e){
            System.out.println(e.getMessage());
        }

    }

    public void threadSleep(int waitTime){
        try{
            Thread.sleep(waitTime);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public int implicitWait(int waitTime){
        driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
        return waitTime;
    }
    
    public  void webDriverWaitForEleVisibility(WebDriver driver,WebElement element){
        WebDriverWait wait = new WebDriverWait(driver,3000);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}
