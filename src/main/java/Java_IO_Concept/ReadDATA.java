package Java_IO_Concept;

import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReadDATA {

    @Test(description = "get data from the file")
    public void getData() throws IOException {



        try {
            Path path = FileSystems.getDefault().getPath("logs","testData.txt");
            Path filePath = path.getFileName();
            System.out.println(filePath);
            File file  = new File("./Logs/testData.txt");
            Reader reader = new FileReader(file);
            int reading =reader.read();
            System.out.println("\n'"+ reading +"'");
            reader.close();
        }finally {
        }
    }
}
