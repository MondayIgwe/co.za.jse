package JWT.com.monday.programmer;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

public class JWT_concept {

    public static void main(String[] args) {
        Instant now = Instant.now();
        byte[] secret = Base64.getDecoder().decode("8njninfsifninb89utwtu08w4won9hw4rn9n");

        String jwt = Jwts.builder()
        .setSubject("monee Dollars")
        .setAudience("People watching")
         .claim("id20",new Random().nextInt(20)+1)
        .setIssuedAt(Date.from(now.plus(1, ChronoUnit.MINUTES)))
        .signWith(Keys.hmacShaKeyFor(secret))
        .compact();

        System.out.println(jwt);
    }
}
