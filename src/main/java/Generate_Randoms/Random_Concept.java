package Generate_Randoms;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.testng.annotations.Test;

import java.util.Random;

public class Random_Concept {

    public static void main(String[] args) throws Exception {
        Random_Concept c = new Random_Concept();
        c.randomInt(3);
    }

  //  @Test
    public String randomString(int length) throws Exception {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
       System.out.println(builder.toString());
        return builder.toString();
    }

   // @Test
    public void givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        System.out.println(generatedString);
    }


    public void randomString(){
        final String ALFA_NUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        int length = 20;
        StringBuilder builder = new StringBuilder();

        while (length-- !=0){
            int index = (int)Math.random()*ALFA_NUMERIC.length();
            builder.append(ALFA_NUMERIC.charAt(index));
        }
        System.out.println(builder.toString() );
    }

    public String randomInt(int length) throws Exception {
        String ALPHA_NUMERIC_STRING = "123456789";

        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        System.out.println( builder.toString());
        return builder.toString();
    }
}
