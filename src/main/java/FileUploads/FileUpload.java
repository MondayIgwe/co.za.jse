package FileUploads;

import TestNG_Demo.TestNG_Listeners;
import commonLibs.CommonLibs;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class FileUpload{

    WebDriver driver;

    @FindBy(css = "input#imagesrc")
     private WebElement choose_File;


    @Parameters({"environment-Suite-Level"})
    @BeforeMethod
    public void setUp() throws Exception {
        //Test_SetUp.setUp("http://demo.automationtesting.in/Register.html");

        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
//        DesiredCapabilities cap = new DesiredCapabilities();
//        cap.setAcceptInsecureCerts(true);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-infobars");
        //options.merge(cap);
        driver = new ChromeDriver(options);
        driver.get("http://demo.automationtesting.in/Register.html");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
    }
    @Test
    public void fileUpload() throws Exception {
        getChooseFile();

        //Robot Class
        Robot robot = new Robot();
        robot.setAutoDelay(2000);

        //The File to be uploaded
        StringSelection stringSelection = new StringSelection("C:\\Users\\MondayI\\IdeaProjects\\co.za.jse\\src\\main\\java\\utils\\data.xml");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection,null);

        robot.setAutoDelay(1000);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.setAutoDelay(1000);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_V);
        robot.setAutoDelay(1000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        robot.setAutoDelay(1000);
        driver.close();
        //Test_SetUp.tearDown();
    }

    public void getChooseFile(){
       PageFactory.initElements(driver,this);
        try{
            CommonLibs commonLibs = new CommonLibs(driver);
            commonLibs.highLighterMethod(driver,choose_File);

           if(!choose_File.isSelected()){
               Actions act = new Actions(driver);
               act.moveToElement(choose_File,0,1).click().perform();
           }

        }catch (NullPointerException e){
            System.out.println(e.getMessage());
        }
    }
}
