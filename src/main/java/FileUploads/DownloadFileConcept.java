package FileUploads;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DownloadFileConcept {

    /*
        Chrome Settings
     */
    WebDriver driver;
    File folder;

    @BeforeMethod
    public void setUp(){
        folder = new File(UUID.randomUUID().toString());  //CREATED A RANDOM FOLDER
        folder.mkdir();
        WebDriverManager.chromedriver().setup();

        // Set custom downloaded file path. When you check content of downloaded file by robot.
        ChromeOptions options = new ChromeOptions();
        Map<String,Object> pref = new HashMap<String, Object>(){{
            put("profile.default_content_setting.popups",0);
            put("download.default_directory", System.getProperty("user.dir") + File.separator + "src\\Test_Doc"+ File.separator + "downloadFiles");
          // put("download.default_directory",folder.getAbsolutePath());
        }};
        options.setExperimentalOption("prefs",pref);
        DesiredCapabilities cap = new DesiredCapabilities().chrome();
        cap.setCapability(ChromeOptions.CAPABILITY,options);
        driver = new ChromeDriver();
    }

    @Test
    public void downLoadConceptTest() throws InterruptedException {
        driver.get("http://the-internet.herokuapp.com/download");
        WebElement lnkSample = driver.findElement(By.linkText("Sample.txt"));
        lnkSample.click();

        Thread.sleep(1000,3);
        File listOfFiles[] = folder.listFiles();
        //Make sure the directory is Not empty
        Assert.assertTrue(true);

        //Make sure the file is not empty
        for(File file : listOfFiles){
            Assert.assertTrue(file.length()>0);
        }

    }

    @AfterMethod
    public void tearDown(){
        driver.close();
        for(File file : folder.listFiles()){
            file.delete();
        }
        folder.delete();

    }
}
