package FileUploads;

import com.codoid.products.exception.FilloException;
import org.junit.Test;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;

public class ExcelSQL {

    Fillo fillo;
    public static String ExcelPath = System.getProperty("user.dir")+"/src/main/java/utils/data.xlsx";
    public String sheet = "ReferenceData";
    public String firsName = null;
    public String lastName = null;
    public String email = null;
    public String password = null;


    @Test
    public void testFillo() throws FilloException {
        fillo = new Fillo();
        Connection connection = fillo.getConnection(ExcelPath);
        String stQuery = "SELECT * FROM "+sheet+"";
        Recordset recordset = connection.executeQuery(stQuery);

            recordset.moveFirst();  //Start from first Row
            firsName = recordset.getField("firstname");
            lastName = recordset.getField("lastname");
            email = recordset.getField("email");
            password = recordset.getField("password");

            while (recordset.next()){
                System.out.println(firsName+ "  " + lastName+ " " + email+ " "+ password);
                System.out.println("\n");
            }


            //Get Columns/Cells
            ArrayList<String> columns = recordset.getFieldNames();
            System.out.println("The column name is " +columns.get(0));

//            int count = 1;
//            for(int i=0;i<count;i++){
//                recordset.moveNext();
//                System.out.println("Move Next function  "+firsName + "  " + lastName + " " + email + " " + password);
//            }


            int sizzz = columns.size();
            System.out.println("The size of the Columns is '"+sizzz+"'");

            int RowCount = recordset.getCount();
             System.out.println("Number of rows is '"+RowCount+"'");

        recordset.close();
        connection.close();
    }

   // @Test
    public void updateExcel() throws FilloException {
        fillo = new Fillo();
        Connection connection = fillo.getConnection(ExcelPath);
        //Where Condition
        String stQuery = "SELECT * FROM "+sheet+" WHERE firstname = 'Frank'";
        Recordset recordset = connection.executeQuery(stQuery);
        while (recordset.next()){
            firsName = recordset.getField("firstname");
            System.out.println(recordset.getField("firstname")+" " +recordset.getField("lastname"));
        }
    }

    public void getDataFromExcel(String sheet) throws FilloException {
        fillo = new Fillo();
        Connection connection = fillo.getConnection(ExcelPath);
        String stQuery = "SELECT * FROM "+sheet+"";
        Recordset recordset = connection.executeQuery(stQuery);

        recordset.moveFirst();  //Start from first Row
        firsName = recordset.getField("firstname");
        lastName = recordset.getField("lastname");
        email = recordset.getField("email");
        password = recordset.getField("password");

        while (recordset.next()){
            System.out.println(firsName+ "  " + lastName+ " " + email+ " "+ password);
            System.out.println("\n");
        }


        //Get Columns/Cells
        ArrayList<String> columns = recordset.getFieldNames();
        System.out.println("The column name is " +columns.get(0));

//            int count = 1;
//            for(int i=0;i<count;i++){
//                recordset.moveNext();
//                System.out.println("Move Next function  "+firsName + "  " + lastName + " " + email + " " + password);
//            }


        int sizzz = columns.size();
        System.out.println("The size of the Columns is '"+sizzz+"'");

        int RowCount = recordset.getCount();
        System.out.println("Number of rows is '"+RowCount+"'");

        recordset.close();
        connection.close();
    }

    //@Test
    public  void createXMLforInstance() throws Exception {
        String Instance = "Firm";
        String fileName="mytest";
        try
        {
            Fillo fillo=new Fillo();
            Connection connection = fillo.getConnection(System.getProperty("user.dir") + "/src/test/resources/field_validation.xlsx");
            String strQuery="SELECT * FROM "+Instance+" where InUse='Yes' and XMLUse='Yes'";
            Recordset recordset=connection.executeQuery(strQuery);

            //Get the Number of rows in the excel
            int Count = recordset.getCount();

            //Prepare the document
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();

            // File root element and Create the elements tag
            Element rootElement = doc.createElement("FILE");
            doc.appendChild(rootElement);

            // Instance root element and Create the elements tag
            Element InstanceRoot = doc.createElement("INSTANCE");
            rootElement.appendChild(InstanceRoot);

            int length;
            // Field elements
            for(int i=0; i<Count; i++)
            {
                recordset.moveNext();

                //Create  tag name in the xml file
                Element fieldName = doc.createElement(recordset.getField("XML_Field"));

                if (recordset.getField("Data_Type").equalsIgnoreCase("String")){
                    length = Integer.valueOf(recordset.getField("Length"));
                //   fieldName.appendChild(doc.createTextNode(randomString(length)));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Enum")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Integer")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("SourcedList")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("StringFixed")){
                    String []Default = recordset.getField("XML_Possible_Values").split(",");
                    fieldName.appendChild(doc.createTextNode(Default[0]));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Double")){
                    String Default = recordset.getField("XML_Possible_Values");
                    fieldName.appendChild(doc.createTextNode(Default));
                }else if(recordset.getField("Data_Type").equalsIgnoreCase("Date")){
                    String Default = recordset.getField("XML_Possible_Values");
                    fieldName.appendChild(doc.createTextNode(Default));
                }
                InstanceRoot.appendChild(fieldName);
            }

            recordset.close();
            connection.close();

            Element fieldName = doc.createElement("Action");
            fieldName.appendChild(doc.createTextNode("0"));
            InstanceRoot.appendChild(fieldName);

            // Write the content into XML file
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(System.getProperty("user.dir") + "/src/test/resources/PositiveUploads/"+fileName));
            Result dest = new StreamResult(System.out);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // Beautify the format of the resulted XML
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(source, result);
//            System.out.println("");
//            System.out.println("_____________XML____________");
//            transformer.transform(source, dest);
//            System.out.println("_____________XML_END____________");
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            throw (ex);
        }
    }
}
