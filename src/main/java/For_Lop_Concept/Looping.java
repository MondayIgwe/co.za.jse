package For_Lop_Concept;

public class Looping {

    public static void main(String[] args) {
        Looping c = new Looping();
        c.returnLoop();
    }

    public void returnLoop(){
        String[] names = new String[4];
        names[0] = "loop";
        names[1] = "langa";
        names[2] = "paul";
        names[3] = "looping";

        for(String str : names){
            int size = str.length();
            String res = str;
            System.out.println(size +"\n"+ res);
        }


        int[] grades = {1,2,3,4,5};

        for(int i=0;i<grades.length;i++){
            int n = grades[i];
            System.out.println("\n"+n);
        }
    }

}
