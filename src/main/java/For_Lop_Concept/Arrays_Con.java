package For_Lop_Concept;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Arrays_Con {

    private int x;
    private double d;

    public Arrays_Con(int x, double u){
            this.x=x;
            d=u;
        }
    public static void main(String[] args) {
        Arrays_Con c = new  Arrays_Con(3,4.89);
        c.arrayCom();
    }

    public int getX() {
        return x;
    }

    public void arrayCom(){
        String parts = "foo-barn@koo";
        String[] splitting =  parts.split("@");
        System.out.println("Return one ==> "+ splitting[0]);

        for (String splitter : splitting){
            System.out.println("Return all in separate ==> "+splitter);
        }

        //Sort Arrays
        System.out.println();
        int[] arr = {9,3,5,8,10,2,3,0};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

    }
}
