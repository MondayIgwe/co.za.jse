package ExtentReportListener;

import TakeScreenshot.TakeScreenshot_Concept;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebDriver;
import org.testng.*;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExtentReporterNG implements IReporter {

    private ExtentReports extent;
    private WebDriver driver;
    private String testName;

    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outputDirectory) {
        extent = new ExtentReports(outputDirectory + File.separator
                + "TradeTesting.html", true);

        for (ISuite suite : suites) {
            Map<String, ISuiteResult> result = suite.getResults();

            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();

                buildTestNodes(context.getPassedTests(), LogStatus.PASS);
                buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
                buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
            }
        }

        extent.flush();
        extent.close();
    }

    private void buildTestNodes(IResultMap tests, LogStatus status) {
        ExtentTest test;

        if (tests.size() > 0) {
            for (ITestResult result : tests.getAllResults()) {
                test = extent.startTest(result.getMethod().getMethodName());

                test.setStartedTime(getTime(result.getStartMillis()));
                test.setEndedTime(getTime(result.getEndMillis()));
//                String path = System.getProperty("user.dir") + File.separator+"/" +
//                        "src/main/java/TakeScreenshot/Screenshots1588176942231.jpg";
                String path = getTestName(testName);
                File destination = new File(path);
                test.log(LogStatus.PASS,test.addScreenCapture(destination.getAbsolutePath()));
                test.log(LogStatus.PASS,test.addScreencast(destination.getAbsolutePath()));

                for (String group : result.getMethod().getGroups())
                    test.assignCategory(group);

                if (result.getThrowable() != null) {
                    test.log(status, result.getThrowable());
                } else {
                    test.log(status, "Test " + status.toString().toLowerCase()
                            + "ed");
                }

                extent.endTest(test);
            }
        }
    }

    private Date getTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.getTimeInMillis();
        return calendar.getTime();
    }
    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }
//    private Date getTime(long millis) {
//        SimpleDateFormat sdf = new SimpleDateFormat("HH_mma");
//        Date resultdate = new Date(System.currentTimeMillis());
//        + sdf.format(resultdate)
//        return sdf.format(resultdate);
//    }

    public String getTestName(String testName){
        this.testName=testName;
        if(testName.startsWith("image"));
        File f = new File(System.getProperty("user.dir")
                +File.separator+"/src/main/java/TakeScreenshot/Screenshots/image1588233853080.jpg");
        String image = f.getAbsolutePath();
        System.out.println(image);
        return image;
    }
}
