package ExtentReportListener;

import TakeScreenshot.TakeScreenshot_Concept;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.List;

public class ExtentReport_Concept {

    private static WebDriver driver;

    public static void main(String[] args) throws IOException, InterruptedException {
        // initialize the HtmlReporter
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("./test-output/extent.html");
        // initialize ExtentReports and attach the HtmlReporter
        ExtentReports extentReports = new ExtentReports();
        extentReports.attachReporter(htmlReporter);

        ExtentTest test = extentReports.createTest("Google Search Test","Sample Descriptions");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        test.log(Status.INFO,"Starting Test Case");
        driver.get("https://www.google.com/");
        driver.manage().window().maximize();
        test.pass("Navigate to google");

        String instance = "INSTANCE";

        driver.findElement(By.name("q")).sendKeys("Automation");
        test.pass("Enter text");
        List<WebElement> cli = driver.findElements(By.xpath("//*[@name='btnK']"));
        cli.get(1).click();
        test.pass("button clicked");
        test.log(Status.INFO,instance);

        String image = TakeScreenshot_Concept.getScreenShots(driver);
        test.addScreenCaptureFromPath(image);

        test.info(instance+" Done");
        extentReports.flush();
        driver.get("C:\\Users\\MondayI\\IdeaProjects\\co.za.jse\\test-output\\extent.html");
        Thread.sleep(2000);
        driver.close();

    }



}


