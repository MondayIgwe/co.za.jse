package TakeScreenshot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class TakeScreenshot_Concept {

    public static  String getScreenShots(WebDriver driver) {
        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;

        File src = takesScreenshot.getScreenshotAs(OutputType.FILE);
        String path = System.getProperty("user.dir") + "/src/main/java/TakeScreenshot/Screenshots/image"+System.currentTimeMillis() +".jpg";
        File destination = new File(path);
        try {
            FileUtils.copyFile(src, destination.getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }
}
