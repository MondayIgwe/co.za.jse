package API;

import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Get_HttpRequest {

    private HttpURLConnection connection;
    BufferedReader reader;
    String line;
    StringBuilder responseContent = new StringBuilder();


    public void connectToEndPoint(final String URL){
        try{
            //Set Up URL
            URL url = new URL(URL);
            connection = (HttpURLConnection) url.openConnection();

            //Request Method
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int statusCode = connection.getResponseCode();
            int responseCode = connection.getResponseCode();
            String contentType = connection.getContentType();
            System.out.println("Content Type is: '"+ contentType +"'"+ "\nResponse Code: '"
                    + responseCode +"'" + "\nconnection Code: '" + statusCode + "'");

            //
            if(statusCode >200){
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while ((line = reader.readLine()) !=null){
                    responseContent.append(line);
                }
            }else {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) !=null){
                    responseContent.append(line);
                }

                reader.close();
            }
            System.out.println(responseContent.toString().trim());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            connection.disconnect();
        }
    }
}
