package API;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Property_SetUp {
     static String URI = null;

    public static void getEnv(){
        try {
            Properties prop = new Properties();
            File file;
            FileInputStream fi = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\main\\java\\API\\apiConfig.properties"));
            prop.load(fi);
            URI = prop.getProperty("URI");
            System.out.println(URI);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
