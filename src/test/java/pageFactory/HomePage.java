package pageFactory;

import com.paulhammant.ngwebdriver.NgWebDriver;
import commonLibs.CommonLibs;
import environments.Base_Setup;
import environments.SetupConfig;
import environments.Test_SetUp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import utils.TestUtils;

import java.io.IOException;
import java.util.List;

public class HomePage extends Base_Setup {

    NgWebDriver ngWebDriver;
    Base_Setup base_setup = new Base_Setup();
    CommonLibs commonLibs = new CommonLibs(base_setup.driver);

    public HomePage(WebDriver driver){
        this.base_setup.driver=driver;
       // super(driver);
        PageFactory.initElements(driver,this);
    }

    By userField = By.xpath("//*[@id='GuiAuthServerLoginView.userNameField']");

    public HomePage getLoginCredentials() throws Exception {
        base_setup.getDriver();
        WebElement user = base_setup.driver.findElement(userField);
        WebDriverWait wait = new WebDriverWait(base_setup.driver, 3000);
        wait.until(ExpectedConditions.visibilityOf(user));
        user.sendKeys(SetupConfig.username);
        base_setup.driver.findElement(By.xpath("//*[@id='GuiAuthServerLoginView.nextButton']")).click();
        commonLibs.threadSleep(2000);
        WebElement loginView = base_setup.driver.findElement(By.cssSelector("div[id='GuiAuthServerLoginView.pamCredentialLayout']"));

        //Login as a different user if already logged In
        if (loginView.isDisplayed()) {
            WebElement switchUserButton = base_setup.driver.findElement(By.cssSelector("div[id='GuiAuthServerLoginView.switchUserButton']"));
            commonLibs.highLighterMethod(base_setup.driver, switchUserButton);
            Actions actions = new Actions(base_setup.driver);
            actions.moveToElement(switchUserButton).click().perform();
            commonLibs.threadSleep(1000);
        } else {
            WebElement pass = base_setup.driver.findElement(By.xpath("//*[@id='passwordField']"));
            pass.sendKeys(SetupConfig.password);
            base_setup.driver.findElement(By.xpath("//*[@id='GuiAuthServerLoginView.nextButton']")).click();
        }

        //click Next Button
        base_setup.driver.findElement(By.xpath("//*[@id='GuiAuthServerLoginView.nextButton']")).click();
        commonLibs.threadSleep(3000);
        WebElement pass = base_setup.driver.findElement(By.xpath("//*[@id='passwordField']"));
        pass.sendKeys(SetupConfig.password);
        commonLibs.threadSleep(3000);
        WebElement login = base_setup.driver.findElement(By.xpath("//*[@id='loginButton']"));
        commonLibs.JavaScriptClickElement(base_setup.driver,login);
        return this;
    }

    public HomePage getLogOut() {
        //Menu LayOut Tab
        commonLibs.threadSleep(3000);
        WebElement mainLayout = base_setup.driver.findElement(By.xpath("//*[@id='common.HeaderComponent.mainLayout']/div/div[21]"));
        commonLibs.highLighterMethod(base_setup.driver, mainLayout);
        commonLibs.threadSleep(1000);
        mainLayout.click();

        //logOut
        commonLibs.threadSleep(2000);
        List<WebElement> window = base_setup.driver.findElements(By.cssSelector("div.popupContent"));
        for (int i = 0; i <= window.size(); i++) {
            window.get(1).isDisplayed();
            commonLibs.highLighterMethod(base_setup.driver, window.get(2));
            WebElement logout = base_setup.driver.findElement(By.xpath("//*[@id='common.UserInfoPanel.mainLayout.logout']"));
            commonLibs.threadSleep(1000);
            commonLibs.JavaScriptClickElement(base_setup.driver, logout);
            commonLibs.threadSleep(1000);
            break;
        }

        commonLibs.generateAlert(base_setup.driver, "You have Successfully Logged Out!....");
        return this;
    }

    @AfterClass
    public void tearDown(ITestResult result) throws IOException {
        TestUtils testUtils = null;
        CommonLibs commonLibs = new CommonLibs(base_setup.driver);
        if(result.getStatus() == ITestResult.FAILURE){
            testUtils.takeScreenshotAtEndOfTest(result.getName().trim(),driver);
            commonLibs.threadSleep(2000);
            base_setup.driver.quit();
        }
    }

}
