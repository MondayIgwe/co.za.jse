package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
                    glue = "stepDefinitions",
                    tags = "@tag",
                    plugin = {"pretty","json:target/cucumber.json"},
                    dryRun = false,
                    strict = false,
                    monochrome = true)
public class Runner {
}
