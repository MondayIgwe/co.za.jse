package TestDataDriven_Concept;

import environments.Base_Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class PassFromExcel_Concept extends Base_Setup {

    @DataProvider
    public Object[][] getDataFromExcel(){
        //Row/Col
        Object[][] data = new Object[1][1];
        data[0][0] = "ReferenceData";
        return  data;
    }

    @Parameters({"environment-Suite-Level"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional String env) throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '"+title+"'");

    }

    @Test(dataProvider = "getDataFromExcel")
    public void RunTest(String sheet) throws Exception {
            PageObj pageObj = new PageObj();
            pageObj.ValidateTest(sheet);
            Thread.sleep(2000);
            driver.close();
    }



}
