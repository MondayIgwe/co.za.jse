package TestDataDriven_Concept;

import cucumber.api.java.gl.E;
import environments.Base_Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Apache_POI2 extends Base_Setup {
    private WebDriver driver = null;
    public static String FIRSTNAME;
    public static String LASTNAME;
    public static String EMAIL;
    public static String PASSWORD;
    public static int Rowcount = 0;
    public static int ColumnCount=0;
    private String sheetName = "ReferenceData";
    private String excelPath = System.getProperty("user.dir")+"/src/test/java/TestDataDriven_Concept/data3.xlsx";


    @Parameters({"environment-Suite-Level"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional String env) throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '" + title + "'");
    }

    @Test
    public void passArguements() throws IOException {
        data(excelPath,sheetName);
        getElements();
}

    public void getElements() {
        WebElement element=null;
        element = driver.findElement(By.xpath("//input[@id='firstname' and @name='firstname']"));
        element.sendKeys(FIRSTNAME);
        element= driver.findElement(By.xpath("//input[@id='lastname' and @name='lastname']"));
        element.sendKeys(LASTNAME);
        element = driver.findElement(By.xpath("//input[@id='email' and @name='email']"));
        element.sendKeys(EMAIL);
        element=driver.findElement(By.xpath("//input[@id='PASSWORD' and @name='PASSWORD']"));
        element.sendKeys(PASSWORD);
    }

    public void data(String excelPath,String sheetName) throws IOException {
        File excelFile = new File(excelPath);
        FileInputStream fis = new FileInputStream(excelFile);

        //Create XSSFWorkbook
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheet(sheetName);

        //Get Numbers of Rows & Cells in the Sheet
        Rowcount = sheet.getPhysicalNumberOfRows();
        ColumnCount = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Total rows are: "+Rowcount+"\nTotal Cell are:"+ColumnCount);

        FIRSTNAME = sheet.getRow(1).getCell(0).getStringCellValue();
        LASTNAME = sheet.getRow(2).getCell(1).getStringCellValue();
        EMAIL = sheet.getRow(3).getCell(2).getStringCellValue();
        PASSWORD = sheet.getRow(4).getCell(3).getStringCellValue();
        System.out.println(FIRSTNAME +" | " +LASTNAME+" | " + EMAIL+" | " + PASSWORD);

        fis.close();
        workbook.close();
    }

    @AfterMethod
    public void tearDown(){
        try{
            Thread.sleep(3000);
            driver.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Test Done!");
        }

    }
}
