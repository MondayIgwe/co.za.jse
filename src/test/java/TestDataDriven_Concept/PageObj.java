package TestDataDriven_Concept;

import com.codoid.products.exception.FilloException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import static TestDataDriven_Concept.ExcelSQL.*;

public class PageObj {

    WebDriver driver = null;
    int maxTries = 0;

    public WebElement getElement(String strXpath) throws InterruptedException {
        WebElement element;

        int count = 0;

        while (true) {
            try {
                element = driver.findElement(By.xpath(strXpath));
                break;
            } catch (InvalidElementStateException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            } catch (StaleElementReferenceException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            } catch (java.util.NoSuchElementException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            } catch (WebDriverException e) {
                Thread.sleep(1000);
                if (++count == maxTries) throw e;
            }
        }

        return element;
    }


    //String lastname,String email,String password

    public void populateField(String fieldName, String datatype, String firstname) throws InterruptedException {
        WebElement element = null;
        //Custom XPATH STRINGS
        String strElement = "//*[contains(@id,'UI_ACTION_ADD:')][contains(@id,'PropertyTable')]/div//table/tbody/tr//td/div[contains(text(),'" + fieldName + "')]/./ancestor::tr//input";
        String strSelectBox = "//*[contains(@id,'UI_ACTION_ADD:')][contains(@id,'PropertyTable')]/div//table/tbody/tr//td/div[contains(text(),'" + fieldName + "')]/./ancestor::tr//div[@class='v-filterselect-button']";
        String strMsdSelectBox = "//*[contains(@id,'UI_ACTION_ADD:')][contains(@id,'PropertyTable')]/div//table/tbody/tr//td/div[contains(text(),'" + fieldName + "')]/./ancestor::tr//div[@class='v-button v-widget MultiSelectCombo-button v-button-MultiSelectCombo-button v-popupbutton']";

        String firstNameString = (("//*[@id='firstname_r']"));

//        getDriverFindElement("//input[@id='lastname' and @name='lastname']").sendKeys(lastname);
//        getDriverFindElement("//input[@id='email' and @name='email']").sendKeys(email);
//        getDriverFindElement("//input[@id='PASSWORD' and @name='PASSWORD']").sendKeys(password);

        if (datatype.equalsIgnoreCase("string")) {
            element = getElement(firstNameString);
            element.sendKeys(firstname);
        } else {
            System.out.println("Not selected, Try AGAIN");
        }
    }

    public void ValidateTest(String sheet) throws FilloException, InterruptedException, IOException {
        ExcelSQL excelSQL = new ExcelSQL();
        excelSQL.getDataFromExcel(sheet);
        int count = RowCount;
        for (int i = 0; i < count; i++) {
            excelSQL.getDataFromExcel(sheet);
            //data(sheet);
            //  populateField(FieldName,DataType,FirstName);
//            WebElement firstName = driver.findElement(By.xpath("//*[@id='firstname_d']"));
//            firstName.sendKeys(FirstName);
        }
    }


    }
