package TestDataDriven_Concept;

import environments.Base_Setup;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Apache_POI extends Base_Setup {
    private WebDriver driver = null;
    public static int Rowcount = 0;
    public static int ColumnCount=0;
    private String sheetName = "ReferenceData";
    private String excelPath = System.getProperty("user.dir")+"/src/test/java/TestDataDriven_Concept/data3.xlsx";


    @Parameters({"environment-Suite-Level"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional String env) throws Exception {
        System.out.println("Initializing Driver for AUT");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://reg.ebay.com/reg/PartialReg");
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        js = ((JavascriptExecutor) driver);
        String title = js.executeScript("return document.title;").toString();
        System.out.println("The page title is '" + title + "'");
    }


    @Test(dataProvider="getData")
    public void TestCase(String firstname,String lastname,String email,String password) throws IOException {
        getElements(firstname,lastname,email,password);
        //System.out.println(firstname + lastname + email + password);
}

    //Pass data into Web element
    public WebElement getElements(String firstName,String lastName,String email,String password) {
        WebElement element=null;
            element = driver.findElement(By.xpath("//input[@id='firstname' and @name='firstname']"));
            element.sendKeys(firstName);
            element= driver.findElement(By.xpath("//input[@id='lastname' and @name='lastname']"));
            element.sendKeys(lastName);
            element = driver.findElement(By.xpath("//input[@id='email' and @name='email']"));
            element.sendKeys(email);
            element=driver.findElement(By.xpath("//input[@id='PASSWORD' and @name='PASSWORD']"));
            element.sendKeys(password);
            return element;
    }

    //Data Provider using TestNG
    @DataProvider(name="getData")
    public Object[][] getData() throws IOException {
        Object[][] testData = getDataFromExcelFile(excelPath,sheetName);
        return testData;
    }

    //Read Data from Excel Sheet
    public Object[][] getDataFromExcelFile(String excelPath,String sheetName) throws IOException {
        File excelFile = new File(excelPath);
        FileInputStream fis = new FileInputStream(excelFile);

        //Create XSSFWorkbook
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheet(sheetName);

        //Get Numbers of Rows in the Sheet
        Rowcount = sheet.getPhysicalNumberOfRows();
        ColumnCount = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Total rows are: "+Rowcount+"\nTotal Cell are:"+ColumnCount);

        //Get data using two dimension array
        Object[][] data = new Object[Rowcount-1][ColumnCount];
        for (int i=1;i<Rowcount;i++) {
            for (int j=0;j<ColumnCount;j++){
                String cellData = sheet.getRow(i).getCell(j).getStringCellValue();

                //Pass cell data into Two Dimension array[index from 0, that why you have subtract 1]
                data[i-1][j] = cellData;
                //System.out.println(celldata);
            }
        }
        fis.close();
        workbook.close();
        return  data;
    }

   @AfterMethod
    public void tearDown(){
        try{
            Thread.sleep(2000);
            driver.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Test Done!");
        }

    }
}
