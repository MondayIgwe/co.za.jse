package stepDefinitions;

import cucumber.api.java.en.*;
import environments.Base_Setup;
import pageFactory.HomePage;

public class EndUserSteps {

    Base_Setup base_setup = new Base_Setup();
    HomePage homePage = new HomePage(base_setup.driver);


    @Given("^User navigate to Yaala login page$")
    public void userNavigateToYaalaLoginPage() throws Exception {
        homePage
                .getLoginCredentials()
                .getLogOut();
    }

    @And("^User enter username and password$")
        public void UserEnterUsernameAndPassword(){
    }

    @Then("^User click login page$")
    public void userClickLoginPage() {
        homePage.getLogOut();
    }
}
